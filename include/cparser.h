/*
A parser for the input of the program

Pietro Marino
*/

#ifndef CPARSER_H
#define CPARSER_H

#include <string>
#include <vector>
#include <map>
#include <iostream>

class CParser
{
public:
	/* constructor */
	CParser(int argc_, char* argv_[]);
	
	/* return the mapped argument */
	std::string GetArg(std::string s);
	
protected:
    CParser(){}

private:
	std::map<std::string, std::string, std::less<std::string>, std::allocator<std::pair<std::string, std::string> > > fdecod;
  
	int fargc;

  std::vector<std::string> fargv;

};

#endif // CPARSER_H
