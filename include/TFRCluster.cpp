
/*!
 *  @author    Alessio Piucci
 *  @brief     Representation of a cluster
 */

//custom libraries
#include "TFRCluster.h"

ClassImp(TFRCluster)

//----------
// Returns the x and y position of the cluster: different for pixel and strips!
//----------
TVectorD TFRCluster::GetMeasure(){

  //for pixel, returns the middle point of the pixel position
  if(this->GetLayer().GetSensorType() == "PIXEL"){
    double measure[2] = {(this->x_min + this->x_max)/2., this->GetY()};
    return TVectorD(2, measure);
  }
  
  //for strip, project the cluster along the direction of the strip
  //same as rotate to coordinate system where the strip is vertical and taking then the new x position
  if(this->GetLayer().GetSensorType() == "STRIP"){
    double dxdy = this->GetLayer().GetDxDy();
    double x = (this->x_min + this->x_max)/2.;
    double y = (this->y_min + this->y_max)/2.;
    double measure[1] = {cos(dxdy)*x - sin(dxdy)*y};
    return TVectorD(1, measure);
  }
  
  //else: ERROR 
  return TVectorD(9);
}

//----------
// Returns the covariance of the measure: different for pixel and strips!
//----------
TMatrixD TFRCluster::GetCovariance(){

  //for pixel, returns 2x2 matrix
  if(this->GetLayer().GetSensorType() == "PIXEL"){
    //first approximation: pixel size as variance
    double mat[4]={pow(this->x_max - this->x_min,2)/12.0,0                              ,
                   0                               ,pow(this->y_max - this->y_min,2)/12.0};
    //Add intrinsic spread of the hit
    mat[0]+=pow(layer.GetHitResolution(),2);
    mat[3]+=pow(layer.GetHitResolution(),2);

    return TMatrixD(2,2,mat);
  }
  
  //for strip, return 1x1 matrix
  if(this->GetLayer().GetSensorType() == "STRIP"){
    //first approximation: cluster size vertical to projection
    double mat[1]={pow(width,2)/12.0};
    //Add intrinsic spread of the hit
    mat[0]+=pow(layer.GetHitResolution(),2);
    
    return TMatrixD(1,1,mat);
  }
  
  double Default[1] ={0};
  
  return TMatrixD(1,1,Default);
}
